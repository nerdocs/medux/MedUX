#!/bin/sh
django-admin makemessages --locale de --ignore=build --ignore=docs --ignore=*.egg-info && django-admin compilemessages --ignore .venv
