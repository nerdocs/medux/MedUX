#!/bin/sh

export REPO="https://gitlab.com/nerdocs/medux/medux.git"
export PROJECT_NAME=medux
export DJANGO_USER=${PROJECT_NAME}
export DATABASE_ENGINE=postgresql
export LOG_FILE=${HOME}/install.log

# =========  subprocedures  =========

die() {
  # echo an error message ($1), and exits with 1
  if [ "$1" != "" ]; then
    echo "ERROR: $1"
  fi
  exit 1
}

render() {
  # renders a given file which may contain $ENV_VARIABLES
  # which will be replaced by their contents, and prints
  # the file
  eval "echo \"$(cat $1)\""
}

try_import() {
  export ${1}="$()"
}

usage() {
  cat << EOF

Usage: $(basename $0) [ -h ]

Options:
    -h        Show usage
EOF

}

# ========= COMMON part ==========

PARSED_ARGUMENTS=$(getopt -o h --long as-user -- "$@")
VALID_ARGUMENTS=$?
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
  exit 2
fi

eval set -- "$PARSED_ARGUMENTS"
while true; do
  case "$1" in
    -h | --help)
      usage;
      exit 2
      ;;
    --as-user)
      # undocumented, used for calling this script as normal user
      shift
      break
      ;;
    --)
      shift
      break
      ;;
    # If invalid options were passed, then getopt should have reported an error,
    # which we checked as VALID_ARGUMENTS when getopt was called...
    *) echo "Unexpected option: $1 - this should not happen."
       usage ;;
  esac
done

# =========  ROOT USER part  =========

if [ "$(whoami)" = "root" ]; then
  export DJANGO_HOME=/home/${DJANGO_USER}/${PROJECT_NAME}

  if [ -f ${DJANGO_HOME}/.env ]; then
    # trying to read SECRET_KEY and DB_PASSWORD from existing .env file"
    SECRET_KEY="$(grep SECRET_KEY ${DJANGO_HOME}/.env | cut -d= -f2)"
    export SECRET_KEY
    DB_PASSWORD="$(grep DATABASE_PASS ${DJANGO_HOME}/.env | cut -d= -f2)"
    export DB_PASSWORD
  fi
  if [ "${SECRET_KEY}" = "" ]; then
    # if nothing worked, create a new SECRET_KEY
    echo "Creating new SECRET_KEY...${SECRET_KEY}"
    SECRET_KEY=$(python3 -c "import secrets; print(secrets.token_urlsafe())")
    # echo "SECRET_KEY=${SECRET_KEY}"
    export SECRET_KEY
  else
    echo "Using Django's SECRET_KEY from .env file."
  fi

  if [ "${DB_PASSWORD}" != "" ]; then
    echo "Using DB_PASSWORD from env."
  else
    DB_PASSWORD=$(python3 -c "import secrets; print(secrets.token_urlsafe())")
    export DB_PASSWORD
    echo "Using database password from environment variable DB_PASSWORD"
  fi

  if [ "${DOMAIN}" = "" ]; then
    while [ "${DOMAIN}" = "" ]; do
      echo -n "Please enter domain name for server: "
      read DOMAIN
    done
  else
    echo "Using domain name from environment variable 'DOMAIN': ${DOMAIN}"
  fi
  # TODO: validate DOMAIN name

  echo "\nInstalling necessary software..."
  apt-get install -y python3 python3-pip python3-virtualenv nginx uwsgi postgresql redis-server >/dev/null

  # mariadb-server mariadb-client python3-dev default-libmysqlclient-dev build-essential

  if egrep -q "^${DJANGO_USER}:.*" /etc/passwd; then
    echo "user ${DJANGO_USER} exists already."
  else
    useradd --create-home --system --user-group --shell /bin/bash ${DJANGO_USER} #>/dev/null 2>&1
  fi


## debug
#  echo "checking variables as root..."
#  echo "REPO: ${REPO}"
#  echo "DJANGO_USER: ${DJANGO_USER}"
#  echo "SECRET_KEY: ${SECRET_KEY}"
#  echo "DB_PASSWORD: ${DB_PASSWORD}"

  # call this script again, but as DJANGO_USER, do user specific tasks there (see below)
  echo "Switching to ${DJANGO_USER} user..."
  export ALLOW_AS_USER=1
  SETUP_SCRIPT_COPY=/home/${DJANGO_USER}/setup.sh
  cp "$0" $SETUP_SCRIPT_COPY
  sudo --preserve-env --set-home -u ${DJANGO_USER} sh $SETUP_SCRIPT_COPY --as-user  || die
  rm $SETUP_SCRIPT_COPY
  unset ALLOW_AS_USER
  # then continue with root tasks...
  echo "Continuing as root user..."

  # install startup files
  cd ${DJANGO_HOME} || die "Could not cd into '${DJANGO_HOME}'."
  render setup/nginx.conf.tpl | tee /etc/nginx/sites-available/${PROJECT_NAME}_nginx.conf >/dev/null
#  render setup/gunicorn.service.tpl | tee /etc/systemd/system/${PROJECT_NAME}_gunicorn.service >/dev/null
  render setup/daphne.service.tpl | tee /etc/systemd/system/${PROJECT_NAME}_daphne.service >/dev/null

  # setup PostGreSQL database
  cd /tmp || die "Could not cd into /tmp"
  # if DB already exists, this will produce an error. But PSQL does not allow "CREATE IF NOT EXISTS".
  sudo -u postgres psql -c "create database ${PROJECT_NAME};"
  sudo -u postgres psql -c "ALTER ROLE ${DJANGO_USER} SET client_encoding TO 'utf-8';"
  sudo -u postgres psql -c "ALTER ROLE ${DJANGO_USER} SET default_transaction_isolation TO 'read committed';"
  sudo -u postgres psql -c "ALTER ROLE ${DJANGO_USER} SET timezone TO 'UTC';"
  sudo -u postgres psql -c "create user ${DJANGO_USER} with encrypted password '${DB_PASSWORD}';"
  sudo -u postgres psql -c "grant all privileges on database ${PROJECT_NAME} to ${DJANGO_USER};"

  # let redis be supervised by systemd
  sudo sed -i "s/^supervised .*/supervised systemd/g" /etc/redis/redis.conf

  systemctl restart redis.service
  # enable nginx/daphne services
  ln -sf /etc/nginx/sites-available/${PROJECT_NAME}.conf /etc/nginx/sites-enabled/
  systemctl enable ${PROJECT_NAME}_daphne.service
#  systemctl enable ${PROJECT_NAME}_gunicorn.service
  systemctl enable ${PROJECT_NAME}_nginx.service
  systemctl restart ${PROJECT_NAME}_daphne.service
#  systemctl restart ${PROJECT_NAME}_gunicorn.service
  systemctl restart ${PROJECT_NAME}_nginx.service

  # ================== Finally... =========================
  echo "Installation finished."
  echo " * Please keep the PostgreSQL database password in a safe place: ${DB_PASSWORD}"


# =========  DJANGO_USER part  =========

elif [ "$(whoami)" = "${DJANGO_USER}" ]; then

  if [ "${ALLOW_AS_USER}" != "1" ]; then
    # "secret" internal switch not triggered...
    die "Please run this script as root user."
  fi

  . $HOME/.bashrc
  # here we can be sure to be ${DJANGO_USER}
  export DJANGO_HOME=${HOME}/${PROJECT_NAME}
  mkdir -p ${DJANGO_HOME} || die "Could not create ${DJANGO_HOME}/"
#   chown ${DJANGO_USER}:${DJANGO_USER} ${DJANGO_HOME}
  cd ~ || die "Could not cd into '$HOME'."

  if [ ! -d .venv ]; then
    echo "Creating virtualenv..."
    virtualenv .venv >/dev/null
  fi

  . .venv/bin/activate
  grep -q "venv/bin/activate" ~/.bashrc || echo ". ${DJANGO_HOME}/.venv/bin/activate" >> ~/.bashrc

  echo "Upgrading pip..."
  pip install --upgrade pip >/dev/null

  echo "Installing ${PROJECT_NAME} locally..."
  pip install ${PROJECT_NAME}[deploy] || die "Could not install ${PROJECT_NAME}[deploy] from PyPi."
  echo "Creating .env file..."
  render setup/.env.example > ${DJANGO_HOME}/.env
#  python src/manage.py collectstatic --noinput
#  python src/manage.py migrate
fi
