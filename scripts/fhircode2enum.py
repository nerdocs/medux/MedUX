#!/usr/bin/python

import sys

if len(sys.argv) == 1:
    print(
        """You have to provide a string with FHIR codes.
Usage: fhircode2enum "code1 | code2 | code3"
prints:
    CODE1 = "code1"
    CODE2 = "code2"
    CODE3 = "code3"
"""
    )
    sys.exit(1)

codes = [code.strip() for code in sys.argv[1].split("|")]

print("(")
for code in codes:
    print(f'    {code.upper().replace("-","_")} = "{code}"')
print(")")
