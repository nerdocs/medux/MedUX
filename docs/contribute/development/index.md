# Contribute Code

In order to be able to contribute code to MedUX there is multiple steps to be taken in advance:

- Setup & configure the project locally
- Get familiar with concepts in MedUX
- Make your first contribution! :tada:

## Setup and configuration

Before we dive deeper into how MedUX works, we want to give you a quickstart on how you can setup the project on your device.  

MedUX is written in Python and uses one of - if not the - most popular web frameworks in the language [**Django**](https://www.djangoproject.com/).  

!!! info "Prerequisites"
    In the following sections we assume you already have Python installed, if that is not the case, please have a look at some of the possible solutions to install it.  

    This guide also requires you to have setup `git` locally and some basics when it comes to using `git`. If you haven't installed `git` or want to have a little introduction, you can head over to [Gitlabs' docs on `git`](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).

---

First you'll clone the repository to your local machine.  
For this you can either visit the [Gitlab repository](https://gitlab.com/nerdocs/medux/medux) and clone from there or enter the following command in a shell in the desired location:

```bash
git clone git@gitlab.com:nerdocs/medux/medux.git
```

Now you can move into the cloned project with `cd medux`. (except you have told `git` to store it elsewhere)  
From within the directory you can create a new virtual environment. ([_What is a virtual environment?_](https://realpython.com/python-virtual-environments-a-primer/#why-do-you-need-virtual-environments))  
You can do so by running:

```bash
python -m venv .venv
```  

At this point Python knows about you wanting to have a _separate space_ for the project (called `.venv` in our case) - but that's not enough - we need to `activate` the environment to work within it. We do that by running `. .venv/bin/activate` in the shell.  

TODO dependencies:   
- [ ] Check that all dependencies are correctly listed in pyproject toml   
- [ ] Document the setup & installation of `medux-common` module and that this is only a temporary constraint


