# User Interface Elements

## Breadcrumbs

Breadcrumbs are rendered using a templatetag `breadcrumb`. Define a "breadcrumbs" block in your base template:

```django
{% block breadcrumbs %}
  {% breadcrumb "Home" "home" %}
{% endblock %}
```
Then override it in your page template:

```django
{% block breadcrumbs %}
  {{ block.super }}
  {% breadcrumb page_title "person:detail" %}
{% endblock %}
```

Don't forget `block.super` - to keep the trail before the current crumb.

## Modal dialogs

To create modal dialogs easily, [Benoit Blanchon](https://blog.benoitblanchon.fr) showed an easy and versatile way of doing them with Django/HTMX.
There is a global `#dialog` element that can be used as `hx-target` to load views in a modal. Just add a link like this:

```django
<button hx-get="{% url 'tenant:add' %}" hx-target="#dialog">
```

and it will open in a modal dialog. You are responsible for the content, it will render everything the view returns within this HTML:

```django
<div id="modal" class="modal">
    <div id="dialog" class="modal-dialog" hx-target="this">
        <!-- your content here -->
    </div>
</div>
```
So make sure you create `modal-header`, `modal-content`, and `modal-footer` yourself appropriately.

This only works well for "displaying" views like `ListView`, `DetailView` or `TemplateView`. For `FormView`s, there is more to do:

### Modal form views

A common pattern is opening a modal dialog using a HTMX view for creating/editing content. As convenience, there are a few helpers to ease that work, [medux.common.api.interfaces.ModalFormViewMixin][] as one of them:

Create a button to open the dialog, as usual:
```django
<button class="btn" hx-get="{% url 'person:add' %}">Add person</button>
```
`person:add` is the name of a CreateView in your urls.py:

```python
# urls.py
urlpatterns = [
    path("add/",views.CreateView.as_view(),name="add"),
]
```
Now create the view, inherit from ModalFormViewMixin, and define a few class attributes:

```python
# views.py
from medux.common.api.interfaces import ModalFormViewMixin
from django.views.generic import CreateView
from .models import Person

class PersonCreateView(ModalFormViewMixin, CreateView):
    model = Person
    # form_class = PersonCreateForm
    # or
    fields = ["first_name", "last_name"]
    modal_title = _("Add new person")
    success_event = "core:person:added"
```

The `ModalFormViewMixin` makes sure that:

* a crispy form helper is created which you can use
* the `modal_title` is passed to the template
* the HttpResponse is empty
* the success_event is triggered after the form is returned

This creates a working modal dialog that contains your view template.
It provides `Cancel` and `Submit` buttons at the footer, and renders your form as **crispy** form. You can customize that form further by extending it:
```django
{% extends 'common/modal-form.html' %}
{% block body %}
 ...form specialities...
{% endblock %}
```

Note:
* always remove the crispy form tags if you use a custom form/crispy helper: `self.helper.form_tags = False`. This is necessary because a form in a modal needs to be outside the "modal-body" tag, so the modal-form itself contains it. Don't render it again with crispy.
* `modal_title` is self-explanatory.
* `success_event` describes an event that is emitted on the client after the form has been submitted successfully. You can react to that specifically: In the code above, `hx-trigger` is set to not only `load`, but also to this special event. So each time the form returns successfully, the list is reloaded. 


#### Client side events after returning

After returning from the modal, the `success_event` on the Javascript side of things is triggered.
You can use that to reload some content. Create a list where the models are displayed in your calling template:

```django
<button class="btn" hx-get="{% url 'person:add' %}">Add person</button>

<div class="list-group" id="people-list"
     hx-get="{% url 'person:list' %}"
     hx-trigger="core:person:added from:body" {# evtl. + ",load once" #}
     hx-disinherit="*">
    {% include 'people_list.html' with object_list=object_list %}
</div>
```
Use the `hx-get` in combination with `hx-trigger="load once"` to reload the content whenever the `core:person:added` event fired,
or pre-fill the first-time content from the list view using `include`. 
The `hx-disinherit` attribute is used, so child elements like buttons don't inherit the custom trigger and keep their natural trigger.

