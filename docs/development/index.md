# General

## Base components

MedUX stands on the shoulders of giants. Make sure you read their documentation, to be familiar with their basics.

* Base
    * [Django](https://djangoproject.com)
  
* UI elements
    * [tabler.io](https://tabler.io)
    * [Bootstrap 5](https://getbootstrap.com)
    * [HTMX](https://htmx.org)


!!! note

    You can edit `.drawio` files using [diagrams.net](https://www.diagrams.net/)
