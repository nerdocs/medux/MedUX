# MedUX - Documentation

MedUX is an Open Source Electronic Medical Record.

!!! warning ""
    Please note that MedUX is currently under development and in a pre-alpha state.  

The tool is envisioned to be a pluggable system that allows for multiple tenants (eg. a general practitioner) to be hosted on one instance of MedUX.  

Pluggable modules are one of the core ideas of MedUX, that should enable providers to customise their installation to the fullest.  
MedUX utilises the [GDAPS plugin system](https://gdaps.readthedocs.io/en/latest/index.html) for the installation of pluggable Django apps so that new plugins can be installed and activated easily..

---


!!! note ""
    The current focus of this documentation site is to enable other contributors to take part in this project.  
    If you are interested in contributing in any way, please head over to: [How to contribute](contribute/index.md)