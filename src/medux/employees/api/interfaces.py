from gdaps.api import Interface

from medux.common.api.interfaces import IHtmxComponentMixin


@Interface
class IEmployeeFormTab(IHtmxComponentMixin):
    params = ["pk"]
    template_name = "employees/adm_employee_tab.html"
