from crispy_forms.layout import Row, Column
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    DetailView,
    ListView,
    UpdateView,
    CreateView,
)

from medux.common.api.interfaces import UseComponentMixin, ModalFormViewMixin
from medux.common.htmx.mixins import HtmxResponseMixin
from medux.employees.api.interfaces import IEmployeeFormTab
from medux.employees.forms import TenantEmployeeForm
from medux.employees.models import Employee, WorkingContract
from medux.common.views import AdministrationPageMixin


class SalaryView(PermissionRequiredMixin, HtmxResponseMixin, DetailView):
    """A view that simply returns the monthly salary that is valid for today's
    contract."""

    permission_required = "employees.view_workingcontract"
    template_name = "employees/workingcontract_salary.html"

    def get_object(self, queryset=None):
        employee = Employee.objects.get(pk=self.kwargs["pk"])
        # we could simply use request.employee instead of asking for the pk
        # but this is another layer of "security", a honeypot that can be preyed
        # if someone wants to see another's salary

        if not self.request.employee == employee:
            raise PermissionError("You can not fetch data from another employee.")
            # TODO: log this in a security log.
        self.contract = employee.active_working_contract(timezone.now().date())
        if self.contract.employee is not employee:
            raise PermissionError(
                f"Contract {self.contract.id} does not belong to employee {employee}."
            )
        return employee

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["working_contract"] = self.contract
        return context


class WorkingContractWidgetView(PermissionRequiredMixin, HtmxResponseMixin, DetailView):
    model = Employee
    template_name = "employees/working_contract_card.html"

    def get_object(self, queryset=None):
        employee = Employee.objects.get(pk=self.kwargs["pk"])
        self.contract = employee.active_working_contract(timezone.now().date())
        if not self.contract:
            return Employee.objects.none()
        if self.contract.employee is not employee:
            raise PermissionError(
                f"Contract {self.contract.id} does not belong to employee {employee}."
            )
        return employee

    def has_permission(self):
        user = self.request.user
        return user.has_perm("employees.view_workingcontract")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["working_contract"] = self.contract
        return context


class WorkingContractFormMixin(
    PermissionRequiredMixin, ModalFormViewMixin, HtmxResponseMixin
):
    fields = [
        "start_date",
        "end_date",
        "employment_date",
        "notice_period_weeks",
        "notice_period_months",
        "classification",
        "intended_application",
        "initial_salary",
        "salary_notice",
        "holiday_year_is_calendar_year",
    ]
    form_layout = [
        Row(
            Column(
                "start_date",
                "end_date",
                "employment_date",
            )
        ),
        Row(
            Column("notice_period_weeks"),
            Column("notice_period_months"),
        ),
        Row(
            Column(
                "classification",
                "intended_application",
                "initial_salary",
                "salary_notice",
                "holiday_year_is_calendar_year",
            )
        ),
    ]

    def get_queryset(self):
        employee = Employee.objects.get(pk=self.kwargs["employee_pk"])
        return WorkingContract.objects.filter(employee=employee)


class WorkingContractUpdateView(WorkingContractFormMixin, UpdateView):
    """A modal view to edit a working contract."""

    model = WorkingContract

    def has_permission(self):
        employee = self.request.user.employee
        return (
            employee.has_perm("employees.change_employee")
            and self.get_object().tenant == employee.tenant
        )

    def get_modal_title(self) -> str:
        return _("Edit working contract of {employee}").format(
            employee=self.get_object().employee
        )


class WorkingContractCreateView(WorkingContractFormMixin, CreateView):
    model = WorkingContract

    def get_modal_title(self) -> str:
        return _("Create working contract for {employee}").format(
            employee=Employee.objects.get(pk=self.kwargs["employee_pk"])
        )

    def has_permission(self):
        employee = self.request.user.employee
        return employee.has_perm("employees.add_employee")


class EmployeeMixin:
    """Mixin that filters the views queryset by the current request's employee"""

    def get_queryset(self):
        return self.model._meta.default_manager.filter(employee=self.request.employee)


class TenantEmployeeViewMixin(AdministrationPageMixin):
    """Mixin that adds some common attributes to the view.

    Can be used for all views that have "Employee" as their model, and use the
    TenantEmployeeForm as form class. Sets permissions and a template name.
    """

    template_name = "employees/adm_tenant_employees.html"
    permission_required = "common.change_tenant"
    model = Employee
    form_class = TenantEmployeeForm


class TenantEmployeeListView(TenantEmployeeViewMixin, ListView):
    title = _("Employees administration")

    def get_queryset(self):
        return self.model._meta.default_manager.filter(
            tenant=self.request.user.tenant
        ).order_by("-is_active", "last_name")


class EmployeeListContextMixin:
    """Mixin that adds the employee list to the context."""

    def get_context_data(self, **kwargs):
        """Add the employee list to the context."""
        context = super().get_context_data(**kwargs)

        context["employee_list"] = self.model._meta.default_manager.filter(
            tenant=self.request.user.tenant,
        ).order_by("-is_active", "last_name")
        return context


class TenantEmployeeUpdateView(
    TenantEmployeeViewMixin,
    EmployeeListContextMixin,
    UseComponentMixin,
    UpdateView,
):
    model = Employee
    components = [IEmployeeFormTab]

    def get_title(self):
        return _("Edit employee: {employee}").format(employee=self.object)


class TenantEmployeeCreateView(
    TenantEmployeeViewMixin, EmployeeListContextMixin, CreateView
):
    title = _("Add employee")

    def form_valid(self, form):
        form.save(commit=False)
        form.instance.tenant = self.request.user.tenant
        return super().form_valid(form)
