from medux.common.api import MeduxPluginAppConfig
from medux.core import __version__
from django.utils.translation import gettext_lazy as _


class EmployeesConfig(MeduxPluginAppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    default = True  # FIXME: Remove when django bug is fixed
    name = "medux.employees"

    groups_permissions = {
        "Users": {
            "employees.WorkSchedule": ["view"],
            "employees.WorkingContract": ["view"],
        },
        "Tenant admins": {
            "employees.Employee": ["view", "add", "change"],
        },
    }

    class PluginMeta:
        verbose_name = _("MedUX employees plugin")
        author = "Christian González"
        author_email = "office@nerdocs.at"
        vendor = "nerdocs"
        description = _("Plugin for Employees base functionality")
        category = _("Core")
        visible = True
        version = __version__
