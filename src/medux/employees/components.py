from django.views.generic import ListView, UpdateView
from django.utils.translation import gettext_lazy as _
from medux.employees.api.interfaces import IEmployeeFormTab
from medux.employees.forms import TenantEmployeeForm
from medux.employees.models import WorkingContract, Employee


class EmployeeFormTabMasterData(IEmployeeFormTab, UpdateView):
    model = Employee
    name = "employee_masterdata"
    template_name = "employees/adm_employee_tab_masterdata.html"
    weight = -10
    title = _("Master data")
    form_class = TenantEmployeeForm
    permission_required = "employees.change_employee"


class EmployeeFormTabContracts(IEmployeeFormTab, ListView):
    name = "employee_contracts"
    template_name = "employees/adm_employee_tab_contracts.html"
    title = _("Contracts")
    model = WorkingContract
    permission_required = "employees.change_employee"

    def get_queryset(self):
        # return WorkingContracts that match the passed user (as pk in the URL)
        return WorkingContract.objects.filter(employee=self.kwargs["pk"]).order_by(
            "-start_date"
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["employee"] = Employee.objects.get(pk=self.kwargs["pk"])
        return context
