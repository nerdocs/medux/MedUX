import pytest

from calendar import MONDAY
from datetime import datetime, time, timezone, date
from django.core.exceptions import ValidationError
from medux.employees.models import WorkSchedule, WorkingTimeRange


@pytest.fixture
def work_schedule1(employee_a1, workingcontract_march_2019):
    # add Mo-Fr 8-15:00 working hours
    ws = WorkSchedule(
        employee=employee_a1,
        working_contract=workingcontract_march_2019,
        start_date=timezone.localdate(datetime(year=2020, month=1, day=1)),
    )
    for weekday in range(MONDAY, 5):
        WorkingTimeRange.objects.create(
            weekday=weekday,
            start_time=time(hour=8, minute=0),
            end_time=time(hour=15, minute=0),
            work_schedule=ws,
        )
    return ws


@pytest.mark.django_db
def test_non_overlapping(
    employee_a1, workschedule_march_2019, workingcontract_march_2019
):
    """test if creation of non-overlapping WorkSchedules raises a ValidationError."""
    # create one in April
    workingcontract_march_2019.save()
    WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_march_2019,
        start_date=datetime(year=2019, month=4, day=1, tzinfo=timezone.utc),
        end_date=datetime(year=2019, month=4, day=30, tzinfo=timezone.utc),
    )
    # create one in February
    WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_march_2019,
        start_date=datetime(year=2019, month=2, day=1, tzinfo=timezone.utc),
        end_date=datetime(year=2019, month=2, day=28, tzinfo=timezone.utc),
    )


@pytest.mark.django_db
def test_overlapping_start(
    employee_a1, workingcontract_march_2019, workschedule_march_2019
):
    workingcontract_march_2019.save()
    april_2019_with_1day_in_march = WorkSchedule(
        employee=employee_a1,
        working_contract=workingcontract_march_2019,
        start_date=date(year=2019, month=3, day=31),
        end_date=date(year=2019, month=4, day=30),
    )
    with pytest.raises(ValidationError):
        april_2019_with_1day_in_march.clean()

    april_2019_with_10days_in_march = WorkSchedule(
        employee=employee_a1,
        working_contract=workingcontract_march_2019,
        start_date=date(year=2019, month=3, day=21),
        end_date=date(year=2019, month=4, day=30),
    )
    with pytest.raises(ValidationError):
        april_2019_with_10days_in_march.clean()


@pytest.mark.django_db
def test_overlapping_year(employee_a1, workingcontract_march_2019):
    workingcontract_march_2019.save()
    WorkSchedule.objects.create(
        employee=employee_a1,
        working_contract=workingcontract_march_2019,
        start_date=date(year=2019, month=1, day=1),
        end_date=date(year=2019, month=1, day=31),
    )

    with pytest.raises(ValidationError):
        december_2018_with_1day_in_jan = WorkSchedule(
            employee=employee_a1,
            working_contract=workingcontract_march_2019,
            start_date=date(year=2018, month=12, day=1),
            end_date=date(year=2019, month=1, day=1),
        )
        december_2018_with_1day_in_jan.clean()


@pytest.mark.django_db
def test_overlapping_end(
    employee_a1, workschedule_march_2019, workingcontract_march_2019
):
    with pytest.raises(ValidationError):
        february_2019_with_1day_in_march = WorkSchedule(
            employee=employee_a1,
            working_contract=workingcontract_march_2019,
            start_date=date(year=2019, month=2, day=1),
            end_date=date(year=2019, month=3, day=1),
        )
        february_2019_with_1day_in_march.clean()

    with pytest.raises(ValidationError):
        february_2019_with_10days_in_march = WorkSchedule(
            employee=employee_a1,
            working_contract=workingcontract_march_2019,
            start_date=date(year=2019, month=2, day=1),
            end_date=date(year=2019, month=3, day=10),
        )
        february_2019_with_10days_in_march.clean()
