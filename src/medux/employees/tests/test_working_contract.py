from datetime import date


import pytest


@pytest.mark.django_db
def test_duration_in_year_march_2019(workingcontract_march_2019):
    workingcontract_march_2019.save()
    assert workingcontract_march_2019.days_in_year(2019).days == 31
    assert workingcontract_march_2019.days_in_year(2020).days == 0
    assert workingcontract_march_2019.days_in_year(2018).days == 0


@pytest.mark.django_db
def test_duration_in_year(
    employee_a1,
    classification_foo,
    application_med_assistant,
    workingcontract_march_2019,
):
    workingcontract_march_2019.start_date = date(2020, 12, 1)
    workingcontract_march_2019.end_date = date(2021, 1, 20)
    assert workingcontract_march_2019.days_in_year(2020).days == 31
    assert workingcontract_march_2019.days_in_year(2021).days == 20
    assert workingcontract_march_2019.days_in_year(2019).days == 0
    assert workingcontract_march_2019.days_in_year(2022).days == 0


@pytest.mark.django_db
def test_emplyment_date(workingcontract_march_2019):
    workingcontract_march_2019.clean()
    workingcontract_march_2019.save()
    assert (
        workingcontract_march_2019.employment_date
        == workingcontract_march_2019.start_date
    )
