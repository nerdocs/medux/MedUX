"""
ASGI config for medux project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/stable/howto/deployment/asgi/
"""
import os
from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter  # noqa: E402
from channels.auth import AuthMiddlewareStack  # noqa: E402

django_asgi_app = get_asgi_application()

from medux.common.urls import websocket_urlpatterns  # noqa: E402

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "medux.settings")

application = ProtocolTypeRouter(
    {
        "http": django_asgi_app,
        "websocket": AuthMiddlewareStack(
            URLRouter(
                websocket_urlpatterns,
                # path("ws/messages", consumers.MyConsumer.as_asgi()),
                # path("ws/notifications", NotificationConsumer.as_asgi()),
            )
        ),
    }
)
