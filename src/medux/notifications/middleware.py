from asgiref.sync import iscoroutinefunction, markcoroutinefunction
from django.http import HttpResponse

from medux.notifications.models import Alert


class AlertMiddleware:
    """Middleware that handles notifications and alerts."""

    async_capable = True
    sync_capable = False

    def __init__(self, get_response):
        self.get_response = get_response
        if iscoroutinefunction(self.get_response):
            markcoroutinefunction(self)

    async def __call__(self, request):
        response: HttpResponse = await self.get_response(request)
        # FIXME: this is not async capable
        #   in Django 5.0 request.auser() could be used.
        if request.user.is_authenticated:
            # get alerts for authenticated user
            request._alerts = Alert.objects.filter(recipient=request.user)
        else:
            # get alerts for all/anonymous users
            request._alerts = Alert.objects.filter(recipient=None)
        return response
