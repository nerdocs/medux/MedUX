from .api import get_alerts


def notifications(request):
    """Return a lazy 'alerts' context variable.

    The 'DEFAULT_MESSAGE_LEVELS' is already done by django.contrib.messages."""
    return {
        "alerts": get_alerts(request),
    }
