# Generated by Django 4.1.4 on 2023-01-09 11:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="Alert",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created",
                    models.DateTimeField(
                        default=django.utils.timezone.now, editable=False
                    ),
                ),
                (
                    "level",
                    models.PositiveIntegerField(
                        choices=[
                            (25, "success"),
                            (10, "info"),
                            (20, "info"),
                            (30, "warning"),
                            (40, "danger"),
                        ],
                        default=20,
                    ),
                ),
                ("message", models.CharField(max_length=1024)),
                (
                    "blocking",
                    models.BooleanField(
                        default=False,
                        help_text="Should the alert block the user's workflow?",
                    ),
                ),
                (
                    "dismissible",
                    models.BooleanField(
                        default=True,
                        help_text="Should the notification be dismissible by the user? "
                        "If not, it will fade away itself.",
                    ),
                ),
                (
                    "recipient",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
