# from django.contrib.auth import get_user_model
# User = get_user_model()
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.views.generic import TemplateView, DetailView

from medux.common.api.interfaces import UseComponentMixin
from medux.core.api import IDashboardSection
from medux.core.models import Patient


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "home.html"
    # FIXME find a correct permission for home, and use MeduxBaseMixin


class PatientFileView(PermissionRequiredMixin, DetailView):
    model = Patient
    context_object_name = "patient"
    permission_required = "can view user"
    template_name = "core/patient_file_detail.html"


class DashboardView(UseComponentMixin, LoginRequiredMixin, TemplateView):
    template_name = "core/dashboard.html"
    components = [IDashboardSection]
