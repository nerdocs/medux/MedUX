import pytest
from medux.core.models import (
    Physician,
    Patient,
    Person,
    MaritalStatusChoices,
    AdministrativeGender,
    Name,
    Specialty,
)
from medux.core.models.patient import PatientRelationship, RelationType


# ========================= Fixtures =========================


@pytest.fixture
def jane():
    jane_name = Name.objects.create(first_name="Jane", last_name="Doe")
    jane = Person.objects.create()
    jane.names.add(jane_name)
    return jane


@pytest.fixture
def john():
    john_name = Name.objects.create(first_name="John", last_name="Doe")
    john = Person.objects.create()
    john.names.add(john_name)
    return john


@pytest.fixture
def male_gender():
    return AdministrativeGender.objects.create(code="male")


@pytest.fixture
def patient(jane, male_gender):
    person = Person.objects.create(birth_date="1990-01-01", gender=male_gender)
    person.names.add(jane.name)
    return Patient.objects.create(
        person=person,
        marital_status=MaritalStatusChoices.DIVORCED,
        emergency_contact_freetext="Contact in case of emergency: 555-555-5555",
    )


@pytest.fixture
def leonard_mccoy():
    return Person.objects.create(first_name="Leonard", last_name="McCoy")


@pytest.fixture
def emh_the_doctor():
    # https://memory-alpha.fandom.com/wiki/The_Doctor
    return Person.objects.create(first_name="The", last_name="Doctor")


@pytest.fixture
def specialty_general():
    return Specialty.objects.create_as_vendor(name="General")


# ========================= Tests =========================


@pytest.mark.django_db
def test_patient_creation(patient):
    """Test patient object is created successfully"""
    assert str(patient.person.name) == "DOE, Jane"
    assert patient.marital_status == MaritalStatusChoices.DIVORCED
    assert (
        patient.emergency_contact_freetext
        == "Contact in case of emergency: 555-555-5555"
    )


@pytest.mark.django_db
def test_str_method(patient):
    """Test __str__ method of Patient model"""
    assert str(patient), "DOE, Jane"


@pytest.mark.django_db
def test_general_practitioner_field(patient, specialty_general, leonard_mccoy):
    """Test general_practitioner field of Patient model"""
    physician = Physician.objects.create(
        person=leonard_mccoy, speciality=specialty_general
    )
    patient.general_practitioner = physician
    patient.save()
    assert patient.general_practitioner == physician


@pytest.mark.django_db
def test_physicians_field(leonard_mccoy, emh_the_doctor, specialty_general, patient):
    """Test physicians field of Patient model"""
    physician1 = Physician.objects.create(
        person=leonard_mccoy, speciality=specialty_general
    )
    physician2 = Physician.objects.create(
        person=emh_the_doctor, speciality=specialty_general
    )
    patient.physicians.add(physician1, physician2)
    physicians = patient.physicians.all()
    assert physician1 in physicians
    assert physician2 in physicians


@pytest.mark.django_db
def test_related_persons_field(jane, patient):
    """Test related_persons field of Patient model"""
    patient1 = Patient.objects.create(person=jane)
    patient2 = Patient.objects.create(
        person=Person.objects.create(first_name="Bob", last_name="Smith")
    )
    friend = RelationType.objects.create(name="Friend")
    PatientRelationship.objects.create(
        from_patient=patient, to_patient=patient1, relation_type=friend
    )
    PatientRelationship.objects.create(
        from_patient=patient, to_patient=patient2, relation_type=friend
    )
    assert set(patient.related_persons.all()) == {patient1, patient2}


@pytest.mark.django_db
def test_emergency_contact_field(patient):
    """Test emergency_contact field of Patient model"""

    contact2 = Person.objects.create()
    contact2.names.add(Name.objects.create(first_name="Bob", last_name="Smith"))
    patient.emergency_contact.add(contact2)
    assert str(patient.emergency_contact.first().name) == "SMITH, Bob"
