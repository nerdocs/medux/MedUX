import pytest
from medux.core.models import (
    Person,
    Name,
)


@pytest.fixture
def jane():
    jane_name = Name.objects.create(first_name="Jane", last_name="Doe")
    jane = Person.objects.create()
    jane.names.add(jane_name)
    return jane


@pytest.fixture
def john():
    john_name = Name.objects.create(first_name="John", last_name="Doe")
    john = Person.objects.create()
    john.names.add(john_name)
    return john


@pytest.mark.django_db
def test_person_name():
    """Test if person object is created successfully"""
    person = Person.objects.create()
    name = Name.objects.create(first_name="John", last_name="Doe")
    person.names.add(name)

    assert str(person.name) == "DOE, John"


@pytest.mark.django_db
def test_person_creation_without_name_model():
    """Test __str__ method of Patient model"""
    person = Person.objects.create(first_name="John", last_name="Doe")
    assert str(person.name) == "DOE, John"
