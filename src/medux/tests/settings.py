"""
Django basic test settings for MedUX project and plugins.

This is in a convenience path, so that in your plugins, you can simply do a

    from medux.tests.settings import *  # noqa

and override necessary settings.
"""

from medux.settings import *  # noqa

SECRET_KEY = "This is only a SECRET_KEY for testing."

DEBUG = True
DEBUG_TOOLBAR = False

ALLOWED_HOSTS = ["127.0.0.1", "localhost"]
# for testing, always use SQLite DB backend.
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "medux-testing.db",
        # "USER": "",
        # "PASSWORD": "",
    }
}

# INSTALLED_APPS.append("tests.testdummy")
