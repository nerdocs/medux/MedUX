# TODO

A short list of things to do in MedUX

### Plugins that should be coded:
* [X] Cashbook
* [ ] Device management
* [X] Employees holidays/time schedule
* [ ] Medication/Prescriptions
* [ ] Online prescriptions
* [ ] Address book for companies, insurances, persons etc.

### Other things
* [ ] use Std Python `tomllib` library instead of `toml`