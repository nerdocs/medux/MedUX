[Unit]
Description=MedUX Gunicorn daemon
After=network.target

[Service]
User=medux
Group=medux
WorkingDirectory=${DJANGO_HOME}
ExecStart=${DJANGO_HOME}/.venv/bin/gunicorn --access-logfile - --workers 3 --bind unix:/run/gunicorn_${DJANGO_USER}.sock ${PROJECT_NAME}.wsgi:application

[Install]
WantedBy=multi-user.target
