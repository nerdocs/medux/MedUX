
# the upstream Django component nginx needs to connect to
upstream channels-backend {
    server localhost:8001;
}

# configuration of the server for assets and websocket
server {
    listen 80;
    listen [::]:80
    # substitute your machine's IP address or FQDN
    server_name ${DOMAIN};

    charset utf-8;
    keepalive_timeout 5;
    location = /favicon.ico { access_log off; log_not_found off; }
    # max upload size
    client_max_body_size 75M;
    error_log ${DOMAIN}-error.log warn;

    # Django media
    location /media  {
        alias /home/${DJANGO_USER}/media;
    }
    location /static {
        alias /home/${DJANGO_USER}/static;
    }

    # pass all / requests to the backend
    location / {
        try_files $uri @proxy_to_app;
    }

    location @proxy_to_app {
        proxy_pass http://backend;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $server_name;
    }

}
